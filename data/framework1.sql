﻿DROP DATABASE IF EXISTS ejemplo2yii;

CREATE DATABASE IF NOT EXISTS ejemplo2yii
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

--
-- Set default database
--
USE ejemplo2yii;

--
-- Create table `coches`
--
CREATE TABLE IF NOT EXISTS coches (
  id int(11) NOT NULL,
  marca varchar(50) DEFAULT NULL,
  fecha date DEFAULT NULL,
  precio float DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `clientes`
--
CREATE TABLE IF NOT EXISTS clientes (
  cod int(11) NOT NULL,
  nombre varchar(50) DEFAULT NULL,
  idCocheAlquilado int(11) DEFAULT NULL,
  fechaAlquiler date DEFAULT NULL,
  PRIMARY KEY (cod)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `idCocheAlquilado` on table `clientes`
--
ALTER TABLE clientes
ADD UNIQUE INDEX idCocheAlquilado (idCocheAlquilado);

--
-- Create foreign key
--
ALTER TABLE clientes
ADD CONSTRAINT fkClientesCoches FOREIGN KEY (idCocheAlquilado)
REFERENCES coches (id) ON DELETE CASCADE ON UPDATE CASCADE;